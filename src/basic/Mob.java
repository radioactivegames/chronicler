package basic;
import java.util.*;

public class Mob {
	
	public String Name;
	public int Level;
	public int Experience;
	public int Strength;
	public int Health;
	public int MaxHealth;
	public int Gold;
	public Random rand;
	
	public Mob(MobType mob) {
		
	}
	
	
	public boolean damage(int dmg) {
		
		rand = new Random();
		double randomDouble = rand.nextDouble();
		int randomInt = (int) (randomDouble * 4);
		Main.console("Random int: "+randomInt);
		dmg = dmg + randomInt;
		Health -= dmg;
		System.out.println(Name+" took "+dmg+" damage.");
		if (Health <= 0) {
			
			Main.console(Name+" has died.");
			return true;
			
		} else {
			
			return false;
			
		}
		
	}
	
}
