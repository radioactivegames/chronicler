package basic;

import java.util.Random;

public class Player {
	
	public String Name;
	public int Level = 1;
	public double Experience = 0;
	public double MaxExperience = 100;
	public int Strength = 3;
	public int Health = 20;
	public int MaxHealth = Health;
	public int Gold = 0;
	public int Kills = 0;
	public Random rand;
	
	//Make a loot Method 
	
	public void xpCheck() {
		
		if (Experience >= MaxExperience) {
			Level++;
			Experience -= MaxExperience;
			MaxExperience = (MaxExperience * 1.4);
			Main.console("Experience: "+Experience);
			Main.console("MaxExperience: "+MaxExperience);
			System.out.println(this+" has leveled up!");
			System.out.println(this+" is now level "+Level);
			
			Strength += 3;
			MaxHealth += 10;
			Health = MaxHealth;
			
		}
		
	}
	
	public boolean damage(int dmg) {
		
		rand = new Random();
		double randomDouble = rand.nextDouble();
		int randomInt = (int) (randomDouble * 4);
		Main.console("Random int: "+randomInt);
		dmg = dmg + randomInt;
		Health -= dmg;
		System.out.println(Name+" took "+dmg+" damage.");
		if (Health <= 0) {
			
			Main.console("Player has died.");
			return true;
			
		} else {
			
			return false;
			
		}
		
	}
	
}
